import logo from './logo.svg';
import './App.css';

let user = {};

async function createUser(data) {
  const response = await fetch(`/api/user`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({user: data})
    });
    try{
  return await response.json();
    }catch(exception){
      
    }
}

function App() {
  return (
    <div className="App">
                <h2>Создание пользователя</h2>
                <form>
                            <label htmlFor="exampleInputEmail1">Логин: </label>
                            <input type="text" onChange={(e) => {user.login = e.target.value;}}  className="form-control" name="login" id="flogin" aria-describedby="emailHelp" placeholder="Логин" /><br/>
                            <label htmlFor="exampleInputPassword1">Пароль: </label>
                            <input type="text" onChange={(e) => {user.password = e.target.value;}} className="form-control" name="password" id="password" placeholder="Пароль" /><br/>
                            <label htmlFor="exampleInputEmail1">Почта: </label>
                            <input type="text" onChange={(e) => {user.email = e.target.value;}} className="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Почта" /><br/>
                    <button type="button" onClick= {(e) => createUser(user)} >Создать</button>
                </form>
    </div>
  );
}

export default App;
